﻿using System;
using System.IO;
using System.Linq;

namespace Google.Extensions
{
    public static class ExtensionMethods
    {
        public static byte[] ReadAllBytes(this Stream stream)
        {
            var bytes = new byte[stream.Length];
            stream.Read(bytes, 0, (int) stream.Length);
            stream.Position = 0;
            return bytes;
        }

        public static string[] SplitLines(this string str)
        {
            return str.Split(new[] {Environment.NewLine, "\n"}, StringSplitOptions.None);
        }

        public static string[] SplitBySpace(this string str)
        {
            return str.Split(' ');
        }

        public static int ToInt(this string str)
        {
            return int.Parse(str);
        }

        public static int[] ConvertToIntArray(this string str)
        {
            return str.SplitBySpace().Select(ToInt).ToArray();
        }
    }
}