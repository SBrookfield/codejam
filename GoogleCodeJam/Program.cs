﻿using System;
using System.Text;
using System.Windows.Forms;
using Google.CodeJam2015;
using Google.Extensions;

namespace Google.CodeJam
{
    static class Program
    {
        [STAThread]
        private static void Main(string[] args)
        {
            Console.WriteLine(new Round1A().ProblemA(LoadSampleFile()));
            Console.ReadKey(true);
        }

        private static string LoadSampleFile()
        {
            var openFileDialog = new OpenFileDialog {Filter = "Google Input File|*.in"};
            openFileDialog.ShowDialog();
            return !String.IsNullOrEmpty(openFileDialog.FileName)
                ? Encoding.Default.GetString(openFileDialog.OpenFile().ReadAllBytes())
                : null;
        }
    }
}