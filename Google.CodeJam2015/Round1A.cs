﻿using System;
using System.Collections.Generic;
using System.Linq;
using Google.Extensions;

namespace Google.CodeJam2015
{
    // Problem A. Mushroom Monster
    // http://code.google.com/codejam/contest/4224486/dashboard

    public class Round1A
    {
        private class TestCase
        {
            public int TestNumber { get; set; }
            public int[] Mushrooms { get; set; }
        }

        public string ProblemA(string inputFile)
        {
            var output = new List<string>();
            foreach (var testCase in GenerateTestCases(inputFile))
                output.Add(string.Format("Case #{0}: {1} {2}", testCase.TestNumber, NumberOfMushroomsEaten(testCase.Mushrooms), NumberOfMushroomsEatenByTime(testCase.Mushrooms)));

            return string.Join(Environment.NewLine, output);
        }

        private int NumberOfMushroomsEaten(IReadOnlyList<int> mushrooms)
        {
            var noOfMushroomsEaten = 0;

            for (var c = 0; c < mushrooms.Count() - 1; c++)
                if (mushrooms[c + 1] < mushrooms[c])
                    noOfMushroomsEaten += mushrooms[c] - mushrooms[c + 1];

            return noOfMushroomsEaten;
        }

        private int NumberOfMushroomsEatenByTime(IReadOnlyList<int> mushrooms)
        {
            var mushroomsEaten = new List<int>();
            for (var c = 0; c < mushrooms.Count() - 1; c++)
                if (mushrooms[c + 1] < mushrooms[c])
                    mushroomsEaten.Add(mushrooms[c] - mushrooms[c + 1]);

            if (mushroomsEaten.Count == 0)
                return 0;

            var maxMushroomsPerSet = mushroomsEaten.Max();
            var numberOfMusroomsEaten = 0;
            for (var c = 0; c < mushrooms.Count() - 1; c++)
                numberOfMusroomsEaten += mushrooms[c] < maxMushroomsPerSet ? mushrooms[c] : maxMushroomsPerSet;

            return numberOfMusroomsEaten;
        }

        private IEnumerable<TestCase> GenerateTestCases(string inputFile)
        {
            var input = inputFile.SplitLines();
            var testCases = new List<TestCase>();

            for (var c = 1; c < input.Count() - 1; c += 2)
            {
                testCases.Add(new TestCase
                {
                    TestNumber = testCases.Count + 1,
                    Mushrooms = input[c + 1].ConvertToIntArray()
                });
            }

            return testCases;
        }
    }
}
